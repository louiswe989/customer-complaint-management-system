import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule,
         MatButtonModule,
         MatSidenavModule,
         MatIconModule,
         MatListModule,
         MatFormFieldModule,
         MatInputModule,
         MatCardModule,
         MatProgressSpinnerModule,
         MatDialogModule,
         MatSelectModule,
         MatTableModule,
         MatPaginatorModule,
         MatSortModule,
         MatRadioModule} from '@angular/material';
import { ChartModule } from 'angular2-chartjs';

import { AppComponent } from './app.component';
import { LoginPageComponent, RegisterDialogComponent } from './login-page/login-page.component';
import { StatisticComponent } from './dashboard/statistic/statistic.component';
import { ComplaintRecordComponent } from './dashboard/complaint-record/complaint-record.component';
import { HeadViewRegisteredAccountComponent } from './dashboard/head-view-registered-account/head-view-registered-account.component';
import { HeadMainNavComponent } from './dashboard/head-main-nav/head-main-nav.component';
import { ComplaintHistoryComponent } from './dashboard/complaint-history/complaint-history.component';
import { ProfileComponent } from './dashboard/profile/profile.component';
import { ComplaintEntryComponent } from './dashboard/complaint-entry/complaint-entry.component';
import { MainNavComponent } from './dashboard/main-nav/main-nav.component';




@NgModule({
  declarations: [
    AppComponent,
    LoginPageComponent,
    RegisterDialogComponent,
    ProfileComponent,
    ComplaintEntryComponent,
    MainNavComponent,
    StatisticComponent,
    ComplaintRecordComponent,
    HeadViewRegisteredAccountComponent,
    HeadMainNavComponent,
    ComplaintHistoryComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    MatProgressSpinnerModule,
    MatDialogModule,
    MatSelectModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatRadioModule,
    ChartModule
  ],
  entryComponents: [RegisterDialogComponent],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
