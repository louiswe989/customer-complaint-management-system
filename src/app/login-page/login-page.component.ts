import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Validators, FormControl } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ApiService } from '../api.service';
import { UserService } from '../providers/user.service';
import { User } from '../interfaces/user';

// login page
@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {

  usernameValidator = new FormControl('', [Validators.required]);
  passwordValidator = new FormControl('', [Validators.required]);
  hide = true;

  user: User;
  json;

  constructor(private api: ApiService,
              private userService: UserService,
              private router: Router,
              public dialog: MatDialog) {
                this.user = {
                  id: '',
                  password: '',
                  name: '',
                  position: '',
                  phone: '',
                  email: '',
                };
              }

  ngOnInit() {
  }

  login() {
    console.log(this.user);

    this.userService.performLogin(this.user).subscribe(
      response => {
        this.json = response;

        this.userService.setToken(this.json.token);
        console.log(this.userService.getToken());

        if ((this.user.id.toUpperCase()).startsWith('AD')) {
          this.router.navigateByUrl('/head-main-nav');
        } else {
          this.router.navigateByUrl('/main-nav');
        }
      },
      (err) => {
        if (err.status === 404) {
          alert('Wrong username or password');
        }
      }
    );
  }

  openRegisterDialog() {
    const dialogRef = this.dialog.open(RegisterDialogComponent, {
      width: '300px',
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  getUsernameErrorMessage() {
    return this.usernameValidator.hasError('required') ? 'You must enter your username' : '';
  }

  getPasswordErrorMessage() {
    return this.passwordValidator.hasError('required') ? 'You must enter your password' : '';
  }

  getVisibility() {
    return this.hide ? 'visibility_off' : 'visibility';
  }
}

// register page - dialog type
@Component({
  selector: 'app-register-dialog',
  templateUrl: './register-dialog.component.html',
  styleUrls: ['./login-page.component.css']
})
export class RegisterDialogComponent {

  emailValidate = new FormControl('', [Validators.email]);
  hide = true;
  user: User;

  constructor(
    public dialogRef: MatDialogRef<RegisterDialogComponent>,
    private userService: UserService
  ) {
    dialogRef.disableClose = true;
    this.user = {
      id: '',
      password: '',
      name: '',
      position: '',
      phone: '',
      email: '',
    };
  }

  getEmailErrorMessage() {
    return this.emailValidate.hasError('email') ? 'Not a valid email' : '';
  }

  getVisibility() {
    return this.hide ? 'visibility_off' : 'visibility';
  }

  onRegisterClick() {
    if (this.user.id !== '' &&
        this.user.password !== '' &&
        this.user.name !== '' &&
        this.user.position !== '' &&
        this.user.phone !== '' &&
        this.user.email !== '' &&
        this.user.id.length === 8) {
      this.userService.addNewUser(
        this.user
      ).subscribe(
        _ => {
          console.log(this.user);
          alert('Successfully registered');
          return this.dialogRef.close();
        },
        (err) => {
          if (err.status === 409) {
            alert('The following ID is registered. Please insert a new ID');
          }
        }
      );
    } else {
      alert('Please fill up all required field with proper format.');
    }
  }

  closeDialog() {
    return this.dialogRef.close();
  }
}
