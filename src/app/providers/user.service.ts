import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from './../interfaces/user';
import { Observable } from 'rxjs';

const backend_url = 'http://127.0.0.1:5000';

const login_endpoint = '/login';

const user_endpoint = '/users';

const TOKEN = 'TOKEN';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(public http: HttpClient) { }

  getAllAccounts(): Observable<User[]> {
    const url = backend_url + user_endpoint;

    return this.http.get<User[]>(url);
  }

  addNewUser(user: User) {
    const url = backend_url + user_endpoint;

    return this.http.post(
      url,
      JSON.stringify(user),
      {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        })
      }
    );
  }

  performLogin(user: User): Observable<string> {
    const url = backend_url + login_endpoint;

    return this.http.post<string>(
      url,
      JSON.stringify(user),
      {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        })
      }
    );
  }

  getSpecificUser(token: string): Observable<User> {
    const url = backend_url + user_endpoint + `/${token}`;

    return this.http.get<User>(url);
  }

  updateUserDetails(token: string, user: User) {
    const url = backend_url + user_endpoint + `/${token}`;

    return this.http.put(
      url,
      JSON.stringify(user),
      {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        })
      }
    );
  }

  deleteAccount(userID) {
    const url = backend_url + user_endpoint + '/' + userID;

    return this.http.delete(url);
  }

  setToken(token: string): void {
    localStorage.setItem(TOKEN, token);
  }

  getToken() {
    return localStorage.getItem(TOKEN);
  }

  isLogged() {
    return localStorage.getItem(TOKEN) != null;
  }
}
