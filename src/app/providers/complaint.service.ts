import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CustomerComplaint } from '../interfaces/complaint';
import { Observable } from 'rxjs';

const backend_url = 'http://127.0.0.1:5000';

const complaint_endpoint = '/complaints';


@Injectable({
  providedIn: 'root'
})

export class ComplaintService {

  constructor(public http: HttpClient) { }

  getAllComplaint(): Observable<CustomerComplaint[]> {
    const url = backend_url + complaint_endpoint;

    return this.http.get<CustomerComplaint[]>(url);
  }

  getSpecificComplaint(category): Observable<CustomerComplaint[]> {
    const url = backend_url + complaint_endpoint + '/' + category;

    return this.http.get<CustomerComplaint[]>(url);
  }

  addComplaint(customerComplaint: CustomerComplaint) {
    const url = backend_url + complaint_endpoint;

    return this.http.post(
      url,
      JSON.stringify(customerComplaint),
      {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        })
      }
    );
  }

  updateComplaint(customerComplaint: CustomerComplaint) {
    const url = backend_url + complaint_endpoint + '/' + customerComplaint.id;

    return this.http.put(
      url,
      JSON.stringify(customerComplaint),
      {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        })
      }
    );
  }

  deleteComplaint(customerComplaint: CustomerComplaint) {
    const url = backend_url + complaint_endpoint + '/' + customerComplaint.id;

    return this.http.delete(url);
  }
}
