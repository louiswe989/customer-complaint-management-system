import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CustomerComplaint } from '../interfaces/complaint';
import { History } from '../interfaces/history';

const backend_url = 'http://127.0.0.1:5000';

const history_endpoint = '/histories';

@Injectable({
  providedIn: 'root'
})
export class HistoryService {

  constructor(public http: HttpClient) { }

  addHistory(customerComplaint: CustomerComplaint) {
    const url = backend_url + history_endpoint;

    return this.http.post(
      url,
      JSON.stringify(customerComplaint),
      {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        })
      }
    );
  }

  getHistories(): Observable<History[]> {
    const url = backend_url + history_endpoint;

    return this.http.get<History[]>(url);
  }

  getBillingCount(): Observable<History[]> {
    const url = backend_url + history_endpoint + '/billing-1';

    return this.http.get<History[]>(url);
  }

  getDisconnectionCount(): Observable<History[]> {
    const url = backend_url + history_endpoint + '/disconnection-2';

    return this.http.get<History[]>(url);
  }

  getServiceCount(): Observable<History[]> {
    const url = backend_url + history_endpoint + '/service-3';

    return this.http.get<History[]>(url);
  }

  getPlanCount(): Observable<History[]> {
    const url = backend_url + history_endpoint + '/plan-4';

    return this.http.get<History[]>(url);
  }

  getTerminationCount(): Observable<History[]> {
    const url = backend_url + history_endpoint + '/termination-5';

    return this.http.get<History[]>(url);
  }

  deleteHistories() {
    const url = backend_url + history_endpoint;

    return this.http.delete(url);
  }
}
