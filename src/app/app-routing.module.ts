import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HeadViewRegisteredAccountComponent } from './dashboard/head-view-registered-account/head-view-registered-account.component';
import { HeadMainNavComponent } from './dashboard/head-main-nav/head-main-nav.component';
import { StatisticComponent } from './dashboard/statistic/statistic.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { ProfileComponent } from './dashboard/profile/profile.component';
import { ComplaintEntryComponent } from './dashboard/complaint-entry/complaint-entry.component';
import { MainNavComponent } from './dashboard/main-nav/main-nav.component';
import { ComplaintHistoryComponent } from './dashboard/complaint-history/complaint-history.component';
import { ComplaintRecordComponent } from './dashboard/complaint-record/complaint-record.component';

import { AuthGuard } from './auth.guard';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full'},
  { path: 'login', component: LoginPageComponent },
  { path: 'head-main-nav', component: HeadMainNavComponent, canActivate: [AuthGuard],
    children: [
      { path: '', redirectTo: 'statistic', pathMatch: 'full'},
      { path: 'statistic', component: StatisticComponent },
      { path: 'complaint-history', component: ComplaintHistoryComponent },
      { path: 'head-view-registered-account', component: HeadViewRegisteredAccountComponent }
    ]
  },
  { path: 'main-nav', component: MainNavComponent, canActivate: [AuthGuard],
    children: [
      { path: '', redirectTo: 'statistic', pathMatch: 'full'},
      { path: 'statistic', component: StatisticComponent },
      { path: 'complaint-record', component: ComplaintRecordComponent },
      { path: 'complaint-entry', component: ComplaintEntryComponent },
      { path: 'complaint-history', component: ComplaintHistoryComponent},
      { path: 'profile', component: ProfileComponent }
    ]
  }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
