import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from '../../interfaces/user';
import { UserService } from '../../providers/user.service';

@Component({
  selector: 'app-main-nav',
  templateUrl: './main-nav.component.html',
  styleUrls: ['./main-nav.component.css'],
})
export class MainNavComponent implements OnInit {

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  user$: Observable<User>;
  user_id;

  constructor(
    private breakpointObserver: BreakpointObserver,
    private userService: UserService
  ) {}

  ngOnInit() {
    this.getUser();
  }

  getUser() {
    this.user$ = this.userService.getSpecificUser(this.userService.getToken());
    this.user$.subscribe(
      response => {
        this.user_id = response.id.toUpperCase();
      }
    );
  }

}
