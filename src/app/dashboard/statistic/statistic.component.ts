import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ComplaintService } from '../../providers/complaint.service';
import { CustomerComplaint } from '../../interfaces/complaint';
import { Chart } from '../../class/chart';

@Component({
  selector: 'app-statistic',
  templateUrl: './statistic.component.html',
  styleUrls: ['./statistic.component.css']
})

export class StatisticComponent implements OnInit {
  complaintsChart = new Chart;
  riskChart = new Chart;

  displayedColumns: string[] = ['position', 'name', 'weight', 'symbol'];

  billing$: Observable<CustomerComplaint[]>;
  disconnection$: Observable<CustomerComplaint[]>;
  service$: Observable<CustomerComplaint[]>;
  plan$: Observable<CustomerComplaint[]>;
  termination$: Observable<CustomerComplaint[]>;

  billing: CustomerComplaint[];
  disconnection: CustomerComplaint[];
  service: CustomerComplaint[];
  plan: CustomerComplaint[];
  termination: CustomerComplaint[];

  constructor(
    private complaintService: ComplaintService
  ) { }

  ngOnInit() {

    this.getComplaintCount();
  }

  getComplaintCount() {
    this.billing$ = this.complaintService.getSpecificComplaint('billing-1');
    this.billing$.subscribe(
      billCount => {
        this.disconnection$ = this.complaintService.getSpecificComplaint('disconnection-2');
        this.disconnection$.subscribe(
          disCount => {
            this.service$ = this.complaintService.getSpecificComplaint('service-3');
            this.service$.subscribe(
              serCount => {
                this.plan$ = this.complaintService.getSpecificComplaint('plan-4');
                this.plan$.subscribe(
                  planCount => {
                    this.termination$ = this.complaintService.getSpecificComplaint('termination-5');
                    this.termination$.subscribe(
                      terCount => {
                        this.showComplaintChart(billCount,
                                                disCount,
                                                serCount,
                                                planCount,
                                                terCount);
                      }
                    );
                  }
                );
              }
            );
          }
        );
      }
    );
  }

  add = (a, b) => a + b.risk;

  showComplaintChart(billCount, disCount, serCount, planCount, terCount) {

    // complaints chart
    this.complaintsChart.type = 'bar';
    this.complaintsChart.data = {
      labels: ['Billing', 'Disconnection', 'Service', 'Plan', 'Termination'],
      datasets: [
        {
          label: 'Quantity',
          data: [billCount.length, disCount.length, serCount.length, planCount.length, terCount.length]
        }
      ]
    };
    this.complaintsChart.options = {
      title: {
        display: true,
        text: 'Total of Complaints',
        fontSize: 20
      },
      responsive: true,
      maintainAspectRatio: false,

      scales: {
        yAxes: [{
            display: true,
            ticks: {
                beginAtZero: true
            }
        }]
      }
    };

    // risk chart
    this.riskChart.type = 'line';
    this.riskChart.data = {
      labels: ['Billing', 'Disconnection', 'Service', 'Plan', 'Termination'],
      datasets: [
        {
          label: 'Risk Level',
          data: [billCount.reduce(this.add, 0),
                 disCount.reduce(this.add, 0),
                 serCount.reduce(this.add, 0),
                 planCount.reduce(this.add, 0),
                 terCount.reduce(this.add, 0)]
        }
      ]
    };
    this.riskChart.options = {
      title: {
        display: true,
        text: 'Risk Level of Complaints',
        fontSize: 20
      },
      responsive: true,
      maintainAspectRatio: false,

      scales: {
        yAxes: [{
            display: true,
            ticks: {
                beginAtZero: true
            }
        }]
      }
    };
  }
}
