import { ComboBox } from './../../class/combo-box';
import { MatTableDataSource, MatSort } from '@angular/material';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { ComplaintService } from '../../providers/complaint.service';
import { UserService } from '../../providers/user.service';
import { HistoryService } from '../../providers/history.service';
import { CustomerComplaint } from '../../interfaces/complaint';
import { User } from '../../interfaces/user';

@Component({
  selector: 'app-complaint-record',
  templateUrl: './complaint-record.component.html',
  styleUrls: ['./complaint-record.component.css']
})
export class ComplaintRecordComponent implements OnInit {

  categories: ComboBox[] = [
    {value: '', viewValue: '*None'},
    {value: 'billing-1', viewValue: 'Billing'},
    {value: 'disconnection-2', viewValue: 'Disconnection'},
    {value: 'service-3', viewValue: 'Service'},
    {value: 'plan-4', viewValue: 'Plan'},
    {value: 'termination-5', viewValue: 'Termination'}
  ];

  complaint$: Observable<CustomerComplaint[]>;
  complaints: CustomerComplaint[];
  user$: Observable<User>;

  displayedColumns: string[] = ['ID', 'category', 'risk', 'name', 'phone', 'email', 'status', 'cs_id', 'registered_datetime'];
  dataSource;

  @ViewChild(MatSort) sort: MatSort;

  selectedCategory;
  selectedRowIndex;
  editing = 0;

  updatedCustomerComplaint: CustomerComplaint;

  constructor(
    private complaintService: ComplaintService,
    private userService: UserService,
    private historyService: HistoryService
  ) {
    this.updatedCustomerComplaint = {
      id: null,
      category: '',
      risk: null,
      name: '',
      phone: '',
      email: '',
      description: '',
      status: '',
      cs_id: '',
      registered_datetime: ''
    };
  }

  ngOnInit() {
    this.fetchComplaints();
  }

  fetchComplaints() {
    this.complaint$ = this.complaintService.getAllComplaint();
    this.complaint$.subscribe(
      getComplaints => {
        this.complaints = getComplaints as CustomerComplaint[];
        this.dataSource = new MatTableDataSource(this.complaints);
        this.dataSource.sort = this.sort;
      }
    );
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  selectedRow(row) {
    if (this.userService.isLogged()) {
      this.user$ = this.userService.getSpecificUser(this.userService.getToken());
      this.user$.subscribe(
      response => {
        if (row.cs_id === response.id) {
          this.selectedRowIndex = row.id;
          this.updatedCustomerComplaint.id = row.id;
          this.updatedCustomerComplaint.category = row.category;
          this.updatedCustomerComplaint.risk = row.risk;
          this.updatedCustomerComplaint.name = row.name;
          this.updatedCustomerComplaint.phone = row.phone;
          this.updatedCustomerComplaint.email = row.email;
          this.updatedCustomerComplaint.description = row.description;
          this.updatedCustomerComplaint.status = row.status;
          this.updatedCustomerComplaint.cs_id = row.cs_id;
          this.updatedCustomerComplaint.registered_datetime = row.registered_datetime;
        } else {
          alert('Warning. Your CS ID must match with the CS ID in registered complaint to perform edit.');
        }
      }
      );
    }
  }

  changeEditState() {
    return this.editing === 1 ? 'SAVE' : 'ENABLE EDITING';
  }

  edit() {
    if (this.editing === 0 &&
      this.updatedCustomerComplaint.name === '' &&
      this.updatedCustomerComplaint.phone === '' &&
      this.updatedCustomerComplaint.description === '') {
        return alert('Please select a complaint to edit');
      } else {
        this.editing = 1;
      }
  }

  save() {
    this.complaintService.updateComplaint(
      this.updatedCustomerComplaint
    ).subscribe(
      _ => {
        alert('Successfully edited customer complaint!');
        this.editing = 0;
        this.fetchComplaints();
      }
    );
  }

  markCaseClosed() {
    if (this.userService.isLogged()) {
      if (this.editing === 1 ||
        this.updatedCustomerComplaint.name === '' ||
        this.updatedCustomerComplaint.phone === '' ||
        this.updatedCustomerComplaint.description === '') {
          return alert('Complaint cannot be closed if the edit field is blank or complaint is under editing.');
        } else {
          this.updatedCustomerComplaint.status = 'Closed';
          this.historyService.addHistory(
            this.updatedCustomerComplaint
          ).subscribe(
            _ => {
              this.complaintService.deleteComplaint(
                this.updatedCustomerComplaint
              ).subscribe(
                response => {
                  console.log(response);
                  alert('The following complaint is closed.');
                  this.updatedCustomerComplaint.id = null;
                  this.updatedCustomerComplaint.category = '';
                  this.updatedCustomerComplaint.risk = null;
                  this.updatedCustomerComplaint.name = '';
                  this.updatedCustomerComplaint.phone = '';
                  this.updatedCustomerComplaint.email = '';
                  this.updatedCustomerComplaint.description = '';
                  this.updatedCustomerComplaint.status = '';
                  this.updatedCustomerComplaint.cs_id = '';
                  this.updatedCustomerComplaint.registered_datetime = '';
                  this.fetchComplaints();
                },
                (err) => {
                  console.log(err.status);
                }
              );
            }
          );
        }
    }
  }
}
