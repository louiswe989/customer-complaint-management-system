import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComplaintRecordComponent } from './complaint-record.component';

describe('ComplaintRecordComponent', () => {
  let component: ComplaintRecordComponent;
  let fixture: ComponentFixture<ComplaintRecordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComplaintRecordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComplaintRecordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
