import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { UserService } from '../../providers/user.service';
import { User } from '../../interfaces/user';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  editing = 0;
  user$: Observable<User>;
  updatedUser: User;

  constructor(private userService: UserService) {
    this.updatedUser = {
      id: '',
      password: '',
      name: '',
      position: '',
      phone: '',
      email: ''
    };
  }

  ngOnInit() {
    this.getUserDetails();
  }

  getUserDetails() {
    if (this.userService.isLogged()) {
      this.user$ = this.userService.getSpecificUser(this.userService.getToken());
      this.user$.subscribe(
        response => {
          this.updatedUser.id = response.id;
          this.updatedUser.password = response.password;
          this.updatedUser.name = response.name;
          this.updatedUser.position = response.position;
          this.updatedUser.phone = response.phone;
          this.updatedUser.email = response.email;
        },
        (err) => {
          if (err.status === 401) {
            alert('Invalid token. Session expired. Please login again');
          }
        }
      );
    }
  }

  changeEditState() {
    return this.editing === 1 ? 'SAVE' : 'ENABLE EDITING';
  }

  save() {
    if (this.userService.isLogged()) {
      if (this.updatedUser.password === '') {
        return alert('Password cannot be blank');
      }
      this.userService.updateUserDetails(
        this.userService.getToken(), this.updatedUser
      ).subscribe(
        _ => {
          alert('Succesfully updated!');
          this.editing = 0;
          this.getUserDetails();
        },
        (err) => {
          if (err.status === 401) {
            alert('Invalid token. Session expired. Please login again');
          }
        }
      );
    }
  }
}
