import { ComboBox } from './../../class/combo-box';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatSort } from '@angular/material';
import { History } from '../../interfaces/history';
import { HistoryService } from '../../providers/history.service';
import { User } from '../../interfaces/user';
import { UserService } from '../../providers/user.service';
import { Chart } from '../../class/chart';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-complaint-history',
  templateUrl: './complaint-history.component.html',
  styleUrls: ['./complaint-history.component.css']
})
export class ComplaintHistoryComponent implements OnInit {
  historyChart = new Chart;
  categories: ComboBox[] = [
    {value: '', viewValue: '*None'},
    {value: 'billing-1', viewValue: 'Billing'},
    {value: 'disconnection-2', viewValue: 'Disconnection'},
    {value: 'service-3', viewValue: 'Service'},
    {value: 'plan-4', viewValue: 'Plan'},
    {value: 'termination-5', viewValue: 'Termination'}
  ];

  selectedCategory;

  historie$: Observable<History[]>;
  histories: History[];

  displayedColumns: string[] = ['ID', 'category', 'risk', 'name', 'phone', 'email', 'status', 'cs_id', 'registered_datetime'];
  dataSource;

  specificHistory: History;
  selectedRowIndex;

  @ViewChild(MatSort) sort: MatSort;

  billing$: Observable<History[]>;
  disconnection$: Observable<History[]>;
  service$: Observable<History[]>;
  plan$: Observable<History[]>;
  termination$: Observable<History[]>;

  user$: Observable<User>;
  enabled = 0;

  constructor(
    private historyService: HistoryService,
    private userService: UserService
  ) {
    this.specificHistory = {
      id: null,
      category: '',
      risk: null,
      name: '',
      phone: '',
      email: '',
      description: '',
      status: '',
      cs_id: '',
      registered_datetime: ''
    };
  }

  ngOnInit() {
    this.fetchHistories();
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  fetchHistories() {
    this.deleteHistoryAuth();
    this.getComplaintCount();

    this.historie$ = this.historyService.getHistories();
    this.historie$.subscribe(
      getHistories => {
        this.histories = getHistories as History[];
        this.dataSource = new MatTableDataSource(this.histories);
        this.dataSource.sort = this.sort;
      }
    );
  }

  selectedRow(row) {
    this.selectedRowIndex = row.id;
    this.specificHistory.id = row.id;
    this.specificHistory.category = row.category;
    this.specificHistory.risk = row.risk;
    this.specificHistory.name = row.name;
    this.specificHistory.phone = row.phone;
    this.specificHistory.email = row.email;
    this.specificHistory.description = row.description;
    this.specificHistory.status = row.status;
    this.specificHistory.cs_id = row.cs_id;
    this.specificHistory.registered_datetime = row.registered_datetime;
  }

  getComplaintCount() {
    this.billing$ = this.historyService.getBillingCount();
    this.billing$.subscribe(
      billCount => {
        this.disconnection$ = this.historyService.getDisconnectionCount();
        this.disconnection$.subscribe(
          disCount => {
            this.service$ = this.historyService.getServiceCount();
            this.service$.subscribe(
              serCount => {
                this.plan$ = this.historyService.getPlanCount();
                this.plan$.subscribe(
                  planCount => {
                    this.termination$ = this.historyService.getTerminationCount();
                    this.termination$.subscribe(
                      terCount => {
                        this.showComplaintChart(billCount.length, disCount.length, serCount.length, planCount.length, terCount.length);
                      }
                    );
                  }
                );
              }
            );
          }
        );
      }
    );
  }

  showComplaintChart(billCount, disCount, serCount, planCount, terCount) {

    // complaint chart
    this.historyChart.type = 'bar';
    this.historyChart.data = {
      labels: ['Billing', 'Disconnection', 'Service', 'Plan', 'Termination'],
      datasets: [
        {
          label: 'Quantity',
          data: [billCount, disCount, serCount, planCount, terCount]
        }
      ]
    };
    this.historyChart.options = {
      title: {
        display: true,
        text: 'Total of Complaints',
        fontSize: 20
      },
      responsive: true,
      maintainAspectRatio: false,

      scales: {
        yAxes: [{
            display: true,
            ticks: {
                suggestedMin: 0,    // minimum will be 0, unless there is a lower value.
                // OR //
                beginAtZero: true   // minimum value will be 0.
            }
        }]
      }
    };
  }

  deleteHistories() {
    this.historyService.deleteHistories().subscribe(
      response => {
        console.log(response);
        alert('Successfully deleted histories!');
        this.getComplaintCount();
        this.fetchHistories();
      },
      (err) => {
        console.log(err.status);
      }
    );
  }

  deleteHistoryAuth() {
    if (this.userService.isLogged()) {
      this.user$ = this.userService.getSpecificUser(this.userService.getToken());
      this.user$.subscribe(
      response => {
        if (response.position === 'Head CS' || response.position === 'Senior CS') {
          this.enabled = 1;
        }
      }
      );
    }
  }

}
