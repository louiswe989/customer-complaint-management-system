import { ComboBox } from './../../class/combo-box';
import { FormControl, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { CustomerComplaint } from '../../interfaces/complaint';
import { ComplaintService } from '../../providers/complaint.service';
import { User } from '../../interfaces/user';
import { UserService } from '../../providers/user.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-complaint-entry',
  templateUrl: './complaint-entry.component.html',
  styleUrls: ['./complaint-entry.component.css']
})
export class ComplaintEntryComponent implements OnInit {

  emailValidate = new FormControl('', [Validators.email]);

  complaints: ComboBox[] = [
    {value: 'billing-1', viewValue: 'Billing'},
    {value: 'disconnection-2', viewValue: 'Disconnection'},
    {value: 'service-3', viewValue: 'Service'},
    {value: 'plan-4', viewValue: 'Plan'},
    {value: 'termination-5', viewValue: 'Termination'}
  ];

  levels: ComboBox[] = [
    {value: 1, viewValue: 'Negligible-1'},
    {value: 2, viewValue: 'Minor-2'},
    {value: 3, viewValue: 'Moderate-3'},
    {value: 4, viewValue: 'Significant-4'},
    {value: 5, viewValue: 'Severe-5'}
  ];

  customerComplaint: CustomerComplaint;
  user$: Observable<User>;

  constructor(
    private complaintService: ComplaintService,
    private userService: UserService
  ) {
    this.customerComplaint = {
      id: null,
      category: '',
      risk: null,
      name: '',
      phone: '',
      email: '',
      description: '',
      status: 'Active',
      cs_id: '',
      registered_datetime: ''
    };
  }

  ngOnInit() {
  }

  getEmailErrorMessage() {
    return this.emailValidate.hasError('email') ? 'Not a valid email' : '';
  }

  onSubmit() {
    if (this.customerComplaint.category !== '' &&
        this.customerComplaint.risk !== null &&
        this.customerComplaint.name !== '' &&
        this.customerComplaint.phone !== '' &&
        this.customerComplaint.email !== '' &&
        this.customerComplaint.description !== '') {
      if (this.userService.isLogged()) {
        this.user$ = this.userService.getSpecificUser(this.userService.getToken());
        this.user$.subscribe(
        response => {
          this.customerComplaint.cs_id = response.id;
          this.customerComplaint.registered_datetime = new Date().toString();

          this.complaintService.addComplaint(
            this.customerComplaint
          ).subscribe(
            _ => {
              alert('Successfully registered new complaint!');
              this.customerComplaint.id = null;
              this.customerComplaint.category = '';
              this.customerComplaint.risk = null;
              this.customerComplaint.name = '';
              this.customerComplaint.phone = '';
              this.customerComplaint.email = '';
              this.customerComplaint.description = '';
              this.customerComplaint.cs_id = '';
              this.customerComplaint.registered_datetime = '';
            }
          );
        }
        );
      }
    } else {
      alert('Please fill up all the required field.');
    }
  }

}
