import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeadViewRegisteredAccountComponent } from './head-view-registered-account.component';

describe('HeadViewRegisteredAccountComponent', () => {
  let component: HeadViewRegisteredAccountComponent;
  let fixture: ComponentFixture<HeadViewRegisteredAccountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeadViewRegisteredAccountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeadViewRegisteredAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
