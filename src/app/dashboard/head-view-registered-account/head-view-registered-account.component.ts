import { ComplaintService } from './../../providers/complaint.service';
import { MatTableDataSource, MatSort } from '@angular/material';
import { Observable } from 'rxjs';
import { User } from './../../interfaces/user';
import { UserService } from './../../providers/user.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ComboBox } from '../../class/combo-box';
import { CustomerComplaint } from 'src/app/interfaces/complaint';

@Component({
  selector: 'app-head-view-registered-account',
  templateUrl: './head-view-registered-account.component.html',
  styleUrls: ['./head-view-registered-account.component.css']
})
export class HeadViewRegisteredAccountComponent implements OnInit {

  positions: ComboBox[] = [
    {value: '', viewValue: '*None'},
    {value: 'Senior CS', viewValue: 'Senior Customer Service'},
    {value: 'Junior CS', viewValue: 'Junior Customer Service'}
  ];

  complaint$: Observable<CustomerComplaint[]>;
  account$: Observable<User[]>;
  accounts: User[];

  displayedColumns: string[] = ['ID', 'name', 'position', 'phone', 'email'];
  dataSource;

  selectedPosition;
  selectedRowIndex;

  specificID;

  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private userService: UserService,
    private complaintService: ComplaintService
  ) { }

  ngOnInit() {
    console.log(this.specificID);
    this.fetchAccounts();
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  fetchAccounts() {
    this.account$ = this.userService.getAllAccounts();
    this.account$.subscribe(
      getAccounts => {
        this.accounts = getAccounts as User[];
        this.dataSource = new MatTableDataSource(this.accounts);
        this.dataSource.sort = this.sort;
      }
    );
  }

  selectedRow(row) {
    if (this.userService.isLogged()) {
      this.selectedRowIndex = row.id;
      this.specificID = row.id;
    }
  }

  deleteAccount() {
    if (this.specificID !== undefined) {
      if (this.userService.isLogged()) {
        this.complaint$ = this.complaintService.getAllComplaint();
        this.complaint$.subscribe(
          getComplaints => {
            if (getComplaints.find(i => i.cs_id === this.specificID)) {
              alert('The following account have active complaint remains.');
            } else {
              this.userService.deleteAccount(this.specificID).subscribe(
                _ => {
                  alert('Successfully delete account!');
                  this.fetchAccounts();
                  this.specificID = undefined;
                },
                (err) => {
                  console.log(err.status);
                }
              );
            }
          }
        );
      }
    } else {
      alert('Please select an account to delete.');
    }
  }
}
