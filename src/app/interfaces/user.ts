export interface User {
  id: string;
  password: string;
  name: string;
  position: string;
  phone: string;
  email: string;
}
