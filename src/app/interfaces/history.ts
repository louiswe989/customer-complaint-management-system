export interface History {
  id: number;
  category: string;
  risk: number;
  name: string;
  phone: string;
  email: string;
  description: string;
  status: string;
  cs_id: string;
  registered_datetime: string;
}
